from django.contrib.auth.models import User, Group
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)

from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ticketmoll.models import (
    Event,
    EventTicket
)
from ticketmoll.serializers import (
    UserSerializer,
    EventTicketSerializer,
    EventSerializer
)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class EventViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows events to be viewed or edited.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class EventTicketViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows event tickets to be viewed or edited.
    """
    queryset = EventTicket.objects.all()
    serializer_class = EventTicketSerializer


# Using email as username
@api_view(['POST'])
def register_view(request):
    username = request.data['username']
    password = request.data['password']

    if User.objects.filter(username=username).exists():
        return Response({
            'error': 'User with that email already exists'
        }, status.HTTP_409_CONFLICT)

    User.objects.create_user(username, username, password)

    return Response()


@api_view(['POST'])
def login_view(request):
    username = request.data['username']
    password = request.data['password']

    user = authenticate(username=username, password=password)

    if user is not None:
        login(request, user)

        return Response()
    else:
        return Response({
            'error': 'Username or password do not match'
        }, status.HTTP_403_FORBIDDEN)


@api_view(['GET'])
def logout_view(request):
    logout(request)

    return Response()


@api_view(['GET'])
def get_user_view(request):
    if (request.user.is_authenticated()):
        return Response({
            'email': request.user.email
        })

    return Response()
