from django.contrib.auth.models import User
from rest_framework import serializers

from ticketmoll.models import Event, EventTicket


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = (
            'id',
            'title',
            'description',
            'img_url',
            'event_type',
            'total_tickets',
            'ticket_price',
            'start_date',
            'end_date',
        )


class EventTicketSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EventTicket
        fields = (
            'event',
            'buyer',
            'sold_date',
        )
