from django.contrib import admin

from ticketmoll.models import (
    Event,
    EventTicket
)


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'event_type',
        'img_url',
        'total_tickets',
        'ticket_price',
        'start_date',
        'end_date',
    )


@admin.register(EventTicket)
class EventTicketAdmin(admin.ModelAdmin):
    pass
