from django.db import models
from django.contrib.auth.models import User


class Event(models.Model):
    CONFERENCE = 1
    CONCERT = 2
    MOVIE = 3

    EVENT_TYPES = (
        (CONFERENCE, 'Conference'),
        (CONCERT, 'concert'),
        (MOVIE, 'movie'),
    )

    title = models.CharField(max_length=1000)
    description = models.CharField(max_length=5000)
    event_type = models.PositiveSmallIntegerField(choices=EVENT_TYPES)
    img_url = models.URLField(max_length=2000)

    total_tickets = models.IntegerField(default=0)
    ticket_price = models.FloatField(default=0.0)

    start_date = models.DateTimeField()
    end_date = models.DateTimeField()


class EventTicket(models.Model):
    event = models.ForeignKey(Event)
    buyer = models.ForeignKey(User)

    sold_date = models.DateTimeField()
