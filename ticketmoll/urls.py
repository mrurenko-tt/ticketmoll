from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from rest_framework import routers

from ticketmoll import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)

router.register(r'events', views.EventViewSet)
router.register(r'events/(?P<id>\d+)/tickets', views.EventTicketViewSet, base_name='event-tickets-list')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^api/v1/register', views.register_view),
    url(r'^api/v1/login', views.login_view),
    url(r'^api/v1/logout', views.logout_view),
    url(r'^api/v1/user', views.get_user_view),
    url(r'^api/v1/', include(router.urls)),
]
