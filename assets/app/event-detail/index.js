module.exports = angular.module(
  'eventDetail',
  [
    'ngRoute',
    'shared.event'
  ]
)
  .component('eventDetail', require('./component'));
