require('./styles.scss');

module.exports = {
  template: require('./template.html'),
  controller: [
    '$routeParams',
    'Event',
    function EventDetailController ($routeParams, Event) {
      this.eventId = $routeParams.eventId;

      this.event = Event.get({ eventId: this.eventId });

      // hall
      // rows
      // seats

      this.rows = 3;
      this.seats = 10;

    }
  ]
};
