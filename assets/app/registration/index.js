module.exports = angular.module(
  'Registration',
  ['ngMessages']
)
  .component('ngRegistration', require('./component'));
