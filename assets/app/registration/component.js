require('./styles.scss');

module.exports = {
  template: require('./template.html'),
  controller: [
    '$scope',
    '$location',
    'auth',
    function ($scope, $location, auth) {
      this.user = {};
      this.isPending = false;

      this.isSubmitDisabled = function () {
        return this.isPending || (this.form.$submitted && this.form.$invalid);
      }.bind(this);

      $scope.$watch(
        function () {
          return this.form.email.$viewValue;
        }.bind(this),
        function () {
          if (this.form) {
            this.form.email.$setValidity('emailUnique', true);
          }
        }.bind(this)
      );

      this.submitForm = function () {
        if (this.form.$invalid) {
          return;
        }

        auth.register(this.user.email, this.user.password).then(
          function () {
            $location.path('/events');
          },
          function (response) {
            // 409 conflict status
            if (response.status === 409) {
              this.form.email.$setValidity('emailUnique', false);
            }
          }.bind(this)
        );
      }.bind(this);

    }],
};
