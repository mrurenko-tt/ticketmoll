module.exports = angular.module(
  'eventListApp',
  ['shared.event']
)
  .component('eventList', require('./component'));
