require('./styles.scss');

module.exports = {
  template: require('./template.html'),
  controller: [
    'Event',
    function EventsListController (Event) {
      var self = this;
      self.orderProp = 'age';

      self.events = Event.query();

      // $http.get('api/v1/events/').then(function (response) {
      //   self.events = response.data;
      // });
    }
  ]
};
