require('./styles.scss');

module.exports = {
  template: require('./template.html'),
  controller: [
    'auth',
    '$location',
    '$scope',
    function (auth, $location, $scope) {
      $scope.isLoading = true;
      $scope.isLoggedIn = false;

      $scope.startLoading = function () {
        this.isLoading = true;
      };

      $scope.stopLoading = function () {
        this.isLoading = false;
      };

      this.updateUserInfo = function () {
        $scope.startLoading();

        auth.fetchUser().then(function (user) {
          $scope.user = user;
          $scope.isLoggedIn = auth.isLoggedIn();

          $scope.stopLoading();
        });
      };

      $scope.user = null;

      $scope.$on("user:login", this.updateUserInfo.bind(this));
      $scope.$on("user:logout", this.updateUserInfo.bind(this));

      this.updateUserInfo();

      $scope.logout = function () {
        auth.logout().then(function () {
          $location.path('/events');
        });
      };
    }
  ]
};
