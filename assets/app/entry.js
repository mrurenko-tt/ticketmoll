var $ = require('expose?$!expose?jQuery!jquery');
require('script!what-input');
require('script!foundation-sites');

require('foundation-sites/dist/foundation.css');
require('./styles/style.scss');

// This is from foundation page template
// Why do we need it? Try to remove later.
$(document).foundation();

require('./app.js');
