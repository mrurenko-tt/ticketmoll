require('./styles.scss');

module.exports = {
  template: require('./template.html'),
  controller: [
    'auth',
    '$location',
    '$scope',
    function (auth, $location, $scope) {
      $scope.error = {};

      this.submitForm = function () {
        if (this.form.$invalid) {
          return;
        }

        auth.login(this.user.email, this.user.password).then(
          function () {
            $scope.error.notValid = false;
            $location.path('/events');
          },

          function (response) {
            if (response.status === 403) {
              $scope.error.notValid = true;
            }
          }.bind(this)
        );

      }.bind(this);

    }
  ]
};
