module.exports = function () {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function (scope, element, attrs, ngModel) {
      var $ctrl = scope.$ctrl;
      var equalKeys = attrs.ngShouldBeEqualTo.split('.');

      var getDictValue = function (dict, keys) {
        var result = dict;

        angular.forEach(keys, function (key) {
          result = result[key];
        });

        return result;

      };

      scope.$watch(
        function () {
          return getDictValue($ctrl, equalKeys);
        },
        ngModel.$validate
      );

      ngModel.$validators.equalTo = function (modelValue, viewValue) {
        var value = modelValue || viewValue;

        if (!value) {
          return false;
        }

        var shouldBeEqualTo = getDictValue($ctrl, equalKeys);

        return value === shouldBeEqualTo;
      };
    },
  };
};
