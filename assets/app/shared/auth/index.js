// Auth service

module.exports = [
  '$http',
  '$q',
  '$rootScope',
  function ($http, $q, $rootScope) {
    this.getUser = function () {
      return this.user;
    };

    this.fetchUser = function (options) {
      options = options || {};

      var deffered = $q.defer();

      if (this.user && !options.forceFetch) {
        deffered.resolve(this.user);
        return deffered.promise;
      }

      $http.get('/api/v1/user').then(function (response) {
        this.user = response.data || null;

        deffered.resolve(this.user);
      }.bind(this));

      return deffered.promise;
    };

    this.register = function (email, password) {
      return $http.post('/api/v1/register', {
        username: email,
        password: password,
      })
        .then(function () {
          this.login(email, password);
        }.bind(this));
    };

    this.login = function (email, password) {
      return $http.post('/api/v1/login', {
        username: email,
        password: password,
      })
        .then(function () {
          this.user = {
            email: email
          };

          $rootScope.$broadcast('user:login');
        }.bind(this));
    };

    this.logout = function () {
      return $http.get('/api/v1/logout').then(function () {
        this.user = null;

        $rootScope.$broadcast('user:logout');
      }.bind(this));
    };

    this.isLoggedIn = function () {
      if (this.user) {
        return true;
      }

      return false;
    };

    this.user = null;
    this.fetchUser();

    return ({
      getUser: this.getUser,
      fetchUser: this.fetchUser,
      register: this.register,
      login: this.login,
      isLoggedIn: this.isLoggedIn,
      logout: this.logout,
    });

  }
];
