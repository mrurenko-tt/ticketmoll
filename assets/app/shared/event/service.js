module.exports = [
  '$resource',
  function ($resource) {
    return $resource('api/v1/events/:eventId', { eventId: '@id' });
  }
];
