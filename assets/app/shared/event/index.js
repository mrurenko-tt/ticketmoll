module.exports = angular.module(
  'shared.event',
  ['ngResource']
)
  .factory('Event', require('./service'));

