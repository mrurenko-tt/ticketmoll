module.exports = angular.module('shared', [
  require('./event').name
])
  .component('ngRequiredLabel', require('./required-label/component'))
  .filter('range', require('./range'))
  .directive('ngShouldBeEqualTo', require('./should-be-equal-to/directive'));
