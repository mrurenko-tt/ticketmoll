var range = require('./index');

describe('Filter: range', function () {

  var rangeFilter = range();

  it('should return same input array if 0 is passed', function () {
    var input = [];
    var input2 = [1, 2, 3];

    expect(rangeFilter(input, 0)).toEqual([]);
    expect(rangeFilter(input, 0, 0)).toEqual([]);

    expect(rangeFilter(input2, -1)).toEqual(input2);
    expect(rangeFilter(input2, -1, -2)).toEqual(input2);
  });

  it('should return same input array if negative params is passed', function () {
    var input = [];
    var input2 = [1, 2, 3];

    expect(rangeFilter(input, -1)).toEqual(input);
    expect(rangeFilter(input, -1, -2)).toEqual(input);

    expect(rangeFilter(input2, -1)).toEqual(input2);
    expect(rangeFilter(input2, -1, -2)).toEqual(input2);
  });

  it('should return right array when "to" param is passed', function () {
    expect(rangeFilter([], 1)).toEqual([0]);
    expect(rangeFilter([], 2)).toEqual([0, 1]);

    expect(rangeFilter([-2, -1], 2)).toEqual([-2, -1, 0, 1]);
  });

});
