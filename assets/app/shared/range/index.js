module.exports = function () {
  return function (input, to) {
    to = parseInt(to, 10);
    var from = 0;

    for (var i = from; i < to; i++) {
      input.push(i);
    }

    return input;
  };
};
