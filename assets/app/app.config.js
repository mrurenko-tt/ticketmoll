module.exports = [
  '$locationProvider',
  '$routeProvider',
  '$httpProvider',
  function config ($locationProvider, $routeProvider, $httpProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider
      .when('/events', {
        template: '<event-list></event-list>'
      })
      .when('/events/:eventId', {
        template: '<event-detail></event-detail>'
      })
      .when('/login', {
        template: '<ng-login></ng-login>'
      })
      .when('/register', {
        template: '<ng-registration></ng-registration>'
      })
      .otherwise('/events');

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  }
];
