var angular = require('angular');
require('angular-route');
require('angular-resource');
require('angular-messages');

var eventsApp = angular.module(
  'eventsApp',
  [
    'ngRoute',
    require('./shared').name,
    require('./main-nav').name,
    require('./login').name,
    require('./registration').name,
    require('./event-list').name,
    require('./event-detail').name,
  ]
);

eventsApp
  .config(require('./app.config'))
  .service('auth', require('./shared/auth'));

module.exports = eventsApp;
