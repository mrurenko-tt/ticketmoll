describe('Event list search', function () {
  describe('event list', function () {

    beforeEach(function () {
      browser.get('http://localhost:8000');
    });

    it('should filter the phone list as a user types into the search box', function () {
      var phoneList = element.all(by.repeater('event in $ctrl.events'));
      var query = element(by.model('$ctrl.query'));

      expect(phoneList.count()).toBe(3);

      query.sendKeys('8');
      expect(phoneList.count()).toBe(1);

      query.clear();
      query.sendKeys('filter2');
      expect(phoneList.count()).toBe(2);
    });

    it('should be possible to control phone order via the drop-down menu', function () {
      var queryField = element(by.model('$ctrl.query'));
      var orderSelect = element(by.model('$ctrl.orderProp'));
      var titleOption = orderSelect.element(by.css('option[value="title"]'));
      var eventTitleColumn = element.all(by.repeater('event in $ctrl.events').column('event.title'));

      function getNames () {
        return eventTitleColumn.map(function (elem) {
          return elem.getText();
        });
      }

      // Let's narrow the dataset to make the assertions shorter
      queryField.sendKeys('filter2');

      expect(getNames()).toEqual([
        'Some concert 3 filter2',
        'Some concert 2 filter2',
      ]);

      titleOption.click();

      expect(getNames()).toEqual([
        'Some concert 2 filter2',
        'Some concert 3 filter2',
      ]);
    });

  });
});
